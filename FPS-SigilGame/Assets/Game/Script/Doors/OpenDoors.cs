﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class OpenDoors : MonoBehaviour
{
    Animator anim;

    public float timeToOpenDoor;

    public bool canOpenDoor;
    void Start()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        Door();
    }
    private void Door()
    {
        if (canOpenDoor == true)
        {
            if (Input.GetKey(KeyCode.F))
            {
                this.timeToOpenDoor -= Time.deltaTime;

                if (this.timeToOpenDoor <= 0)
                {
                    anim.Play("DoorAnimController");
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canOpenDoor = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canOpenDoor = false;
        }
    }
}
