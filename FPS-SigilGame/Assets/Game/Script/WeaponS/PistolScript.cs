﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolScript : MonoBehaviour
{
    #region Singleton;

    public static PistolScript instance;

    #endregion;

    public GameObject bullet;

    public float shootForce, upwardForce;

    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, maxAmmo, actualMaxAmmo, bulletPerTap;
    public bool allowButtonHold;
    public int bulletsLeft, bulletsShot;

    public bool shooting, readyToShoot, reloading;

    public Camera fpsCam;
    public Transform attackPoint;

    public bool allowInvoke = true;


    private void Awake()
    {
        instance = this;
        bulletsLeft = magazineSize;

        maxAmmo = magazineSize * 5;
        actualMaxAmmo = maxAmmo;

        readyToShoot = true;
    }

    private void Update()
    {
        if (WeaponSwitcher.instance.armaEnMano == 1)
        {
            MyInput();
        }

    }
    private void MyInput()
    {
        if (allowButtonHold)
        {
            shooting = Input.GetKey(KeyCode.Mouse0);
        }
        else
        {
            shooting = Input.GetKeyDown(KeyCode.Mouse0);
        }


        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
        {
            Reload();
        }
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0)
        {
            Reload();
        }

        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletsShot = 0;

            Shoot();
        }
    }
    private void Shoot()
    {
        readyToShoot = false;

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit hit;
        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
        {
            targetPoint = ray.GetPoint(75);
        }

        Vector3 directionWithOutSpread = targetPoint - attackPoint.position;


        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 directionWithSpread = directionWithOutSpread + new Vector3(x, y, 0);

        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);
        currentBullet.transform.forward = directionWithSpread.normalized;
        currentBullet.tag = "PistolBullet";
        Destroy(currentBullet, 3f);

        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);

        Debug.DrawRay(attackPoint.position, attackPoint.forward * 10);

        bulletsLeft--;
        bulletsShot++;


        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;
        }

        if (bulletsShot < bulletPerTap && bulletsLeft > 0)
        {
            Invoke("Shoot", timeBetweenShots);
        }
    }
    private void ResetShot()
    {
        readyToShoot = true;
        allowInvoke = true;
    }
    private void Reload()
    {
        reloading = true;
        Invoke("ReloadFinished", reloadTime);
    }
    private void ReloadFinished()
    {
        if (bulletsLeft <= 0)
        {
            bulletsLeft = magazineSize;
            actualMaxAmmo -= bulletsLeft;
        }
        if (bulletsLeft > 0)
        {
            int a = magazineSize - bulletsLeft;

            if (actualMaxAmmo < a)
            {
                bulletsLeft += actualMaxAmmo;
            }
            else
            {
                bulletsLeft += a;
                actualMaxAmmo -= a;
            }
        }
        reloading = false;
    }
}
